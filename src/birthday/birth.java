package birthday;
import java.util.Scanner;

public class birth {

	public static void main(String[] args) {
		//cpjrytyr yjutugkh
		 int n = 3; // Number of disks 
		 TowerofHanoi(n, 'A', 'C', 'B'); // A , B, C are the labels of the rod
	}
	
	static void  TowerofHanoi(int n, char from_rod,  char to_rod, char aux_rod) { 	
		
		//step 1
			if (n == 1) 
			{ 
				System.out.println("Move disk 1 from rod "+  from_rod+" to rod "+to_rod); 
				return; 
			} 
		 //step 2
			TowerofHanoi(n - 1, from_rod, aux_rod, to_rod); 
		 //step 3
			System.out.println("Move disk "+ n + " from rod " + from_rod +" to rod " + to_rod ); 
		 //step 4	
			TowerofHanoi(n - 1, aux_rod, to_rod, from_rod); 
		} 

}